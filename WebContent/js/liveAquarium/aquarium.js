/**
 * Generic function to generate random between intervals
 * @param min Start interval
 * @param max End interval
 * @returns A random integer
 */
function randBetween(min,max) {
    return Math.floor(Math.random()*(max-min+1)+min);
}


$(function() {
	
var especeImg = new Array();
	
$(".fish").each(function() {
	
	// for every fish, the html id is : "fish_INDEX_SPECIE"
	var id = $(this).attr("id").split("_")[1];
	var espece = $(this).attr("id").split("_")[2];
	
	// random generate of an animation with the name : "swimID" (library : jquery.keyframes)
	var fromToPosition = randBetween(10,100);
	$.keyframe.define([{
	    name: 'swim' + id,
		'from' : {'left' : fromToPosition + '%' , 'width' : '42px'},
	    '20%': {'left': '0','transform' : 'scaleX(1)','z-index' : '90'},
		'20.5%': {'transform' : 'scaleX(-1)','z-index' : '10','left': '-30px'},
		'21%': {'left': '80px'},
	    '42%': {'left': '60%'},
	    '50%': {'z-index' : '28','top' : (randBetween(30,70)) + '%','left' : (randBetween(60,75)) + '%','transform' : 'scaleX(-1)'},
		'50.5%': {'z-index' : '100','top' : (randBetween(30,70)) + '%','left' : (randBetween(60,75)) + '%','transform' : 'scaleX(1)'},
		'51%': {'left': (randBetween(10,70)) + '%','z-index' : '150'},
		'60%': {'left': (randBetween(10,70)) + '%','z-index' : '150'},
		'64%': {'left': (randBetween(10,70)) + '%','z-index' : '150'},
		'66%': {'left': (randBetween(10,70)) + 'px','z-index' : '150'},
		'69%': {'left': '-30px','z-index' : '150'},
		'70%': {'z-index' : '90','top' : (randBetween(15,40)) + '%','left' : '-30px','transform' : 'scaleX(1)'},
		'70.5%': {'z-index' : '10','left' : '-30px','transform' : 'scaleX(-1)'},
		'71%': {'left': (randBetween(10,70)) + 'px'},
		'80%': {'left': (randBetween(10,70)) + '%'},
		'81%': {'left': (randBetween(10,70)) + '%','top': (randBetween(40,70)) + '%'},
		'82%': {'left': (randBetween(10,80)) + '%','transform' : 'scaleX(-1)'},
		'82.5%': {'left': (randBetween(60,80)) + '%','top': '45%','transform' : 'scaleX(1)'},
	    '84%': {'z-index' : '28','top' : (randBetween(50,70)) + '%','left' : (randBetween(60,80)) + '%'},
		'88%': {'z-index' : '28','top' : (randBetween(50,70)) + '%','left' : (randBetween(60,80)) + '%'},
		'to': {'left': fromToPosition + '%'}
	}]);
	
	/**
	 * Block to define images for every fishes
	 */
	var images = ["blue-fish.png","gold-fish.png","gold-fish2.png","green-fish.png","tropical-fish.png","whale.png"];
	
	// To be sure that every fish of the same specie have the same image
	if (especeImg[espece] == null) {
		especeImg[espece] = images[randBetween(0,images.length - 1)];
	}
	
	// set the corresponding image to the fish
	$("#img-fish_" + id).attr('src','./img/aquarium/' + especeImg[espece]);
	
	$(this).css("top",randBetween(10,70) + "%");
	
	// Play the animation generated "swimID" with a random speed
	$(this).playKeyframe(
		    "swim" + id + " "+ randBetween(20,40) + "s linear 0s infinite normal forwards",
		    function() {}
	);
});

});