/**
 * The script to init the two charts (saved and not saved)
 * @returns
 */
$(function() {

	var aquaPie = new aquariumPie("fish_chart_saved","chart-data");
	aquaPie.buildData(true);
	if (aquaPie.getSize() > 0) {
		aquaPie.showPie();
	} else {
		$("#canvas-container-saved").html("<img src='./img/empty-aquarium.png' style='width:150px;height:150px;float:left;margin-left:100px;' />");
	}
	
	
	var aquaPieNotSaved = new aquariumPie("fish_chart_not_saved","chart-data");
	aquaPieNotSaved.buildData(false);
	if (aquaPie.getSize() > 0) {
		aquaPieNotSaved.setPieColors(aquaPie.getPieColors());
	}
	if (aquaPieNotSaved.getSize() > 0) {
		aquaPieNotSaved.showPie();
	} else {
		$("#canvas-container-not-saved").html("<img src='./img/empty-aquarium.png' style='width:150px;height:150px;float:right;margin-right:100px;' />");
	}
	
})
