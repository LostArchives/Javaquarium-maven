/**
 * Script to init the chart on details page
 * @returns
 */
$(function() {
	var mydetailPie = new detailPie("fish_chart_detail","chart-data");
	mydetailPie.buildData();
	if (mydetailPie.getSize() > 0) {
		mydetailPie.showPie();
		$("#total_fish").text("Total : " + mydetailPie.getTotalFish());
	} else {
		$("#canvas-container").html("<img src='./img/empty-aquarium.png' style='width:150px;height:150px;float:left;margin-left:100px;' />");
	}
})
