<%@taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>

<bean:parameter id="page_title" name="title" />
<bean:parameter id="page_title_arg" name="title_arg" value="undefined" />

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="./css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="./css/flags/flags.min.css">
<link rel="stylesheet" type="text/css" href="./css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="./css/font-awesome-animation.min.css">
<link rel="stylesheet" type="text/css" href="./css/style.css">

<logic:notEqual name="title_arg" value="undefined">
<title><bean:message key="<%=page_title%>" arg0="<%=page_title_arg%>" /></title>
</logic:notEqual>

<logic:equal name="title_arg" value="undefined">
<title><bean:message key="<%=page_title%>" /></title>
</logic:equal>

