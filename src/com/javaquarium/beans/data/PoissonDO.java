package com.javaquarium.beans.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * JavaBean class to represent a PoissonDO
 * 
 * @author Valentin
 *
 */
@Entity
@Table(name = "t_poisson")
public class PoissonDO {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "c_id")
	private int id;

	@Column(name = "c_couleur", nullable = false)
	private String couleur;

	@Column(name = "c_description", nullable = true)
	private String description;

	@Column(name = "c_largeur", nullable = false)
	private float largeur;

	@Column(name = "c_longueur", nullable = false)
	private float longueur;

	@Column(name = "c_nom", nullable = false, unique = true)
	private String nom;

	@Column(name = "c_prix", nullable = true)
	private int prix;

	/**
	 * @return the id
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(final int id) {
		this.id = id;
	}

	/**
	 * @return the couleur
	 */
	public String getCouleur() {
		return this.couleur;
	}

	/**
	 * @param couleur
	 *            the couleur to set
	 */
	public void setCouleur(final String couleur) {
		this.couleur = couleur;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * @return the largeur
	 */
	public float getLargeur() {
		return this.largeur;
	}

	/**
	 * @param largeur
	 *            the largeur to set
	 */
	public void setLargeur(final float largeur) {
		this.largeur = largeur;
	}

	/**
	 * @return the longueur
	 */
	public float getLongueur() {
		return this.longueur;
	}

	/**
	 * @param longueur
	 *            the longueur to set
	 */
	public void setLongueur(final float longueur) {
		this.longueur = longueur;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return this.nom;
	}

	/**
	 * @param nom
	 *            the nom to set
	 */
	public void setNom(final String nom) {
		this.nom = nom;
	}

	/**
	 * @return the prix
	 */
	public int getPrix() {
		return this.prix;
	}

	/**
	 * @param prix
	 *            the prix to set
	 */
	public void setPrix(final int prix) {
		this.prix = prix;
	}

	@Override
	public String toString() {
		return "Type : " + getClass().getName() + "\n" + "Id : " + this.id + "\n" + "Nom : " + this.nom + "\n"
				+ "Couleur : " + this.couleur + "\n" + "Description : " + this.description + "\n" + "Prix : "
				+ this.prix + "\n" + "Longueur : " + this.longueur + "\n" + "Largeur : " + this.largeur + "\n";
	}

}
