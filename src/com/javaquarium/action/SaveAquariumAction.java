package com.javaquarium.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.javaquarium.beans.data.AquariumPoissonVO;
import com.javaquarium.beans.data.UserVO;
import com.javaquarium.consts.Forward;
import com.javaquarium.consts.MessageKey;
import com.javaquarium.consts.MessageType;
import com.javaquarium.consts.SessionVar;
import com.javaquarium.exception.ServiceException;
import com.javaquarium.util.CatalogueService;

/**
 * Action to commit the operations applied to the aquarium
 * 
 * @author Valentin
 *
 */
public class SaveAquariumAction extends Action {

	/**
	 * The catalogueService to instanciate any services (by Spring IoC)
	 */
	private CatalogueService catalogueService;

	/**
	 * @param catalogueService
	 *            the catalogueService to set
	 */
	public void setCatalogueService(final CatalogueService catalogueService) {
		this.catalogueService = catalogueService;
	}

	/**
	 * Enum to describe the different actions occurring in the execute method (make
	 * code easier to understand)
	 * 
	 * @author Valentin
	 *
	 */
	private enum SaveAction {
		NONE, ADD, UPDATE, REMOVE
	}

	@SuppressWarnings("unchecked")
	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest req,
			final HttpServletResponse res) {

		final HttpSession session = req.getSession();
		final ActionMessages messages = new ActionMessages();
		final Map<Integer, AquariumPoissonVO> myAquarium = (Map<Integer, AquariumPoissonVO>) session
				.getAttribute(SessionVar.MY_AQUARIUM.toString());
		final UserVO user = (UserVO) session.getAttribute(SessionVar.USER.toString());

		try {
			if (myAquarium != null) {
				for (final AquariumPoissonVO apVO : myAquarium.values()) {
					switch (getSaveAction(apVO)) {
					case ADD:
						this.catalogueService.getAquariumPoissonService().addNewAquariumPoisson(user, apVO.getEspece(),
								apVO.getQuantityNotSaved());
						break;
					case UPDATE:
						this.catalogueService.getAquariumPoissonService().updateAquariumPoisson(apVO, user);
						break;
					case REMOVE:
						this.catalogueService.getAquariumPoissonService().removeAquariumPoisson(apVO, user);
						break;
					case NONE:
						break;
					default:
						break;
					}
				}
				messages.add(MessageType.SUCCESS.toString(), new ActionMessage(MessageKey.SUCCESS_SAVE_AQUARIUM));
				// to force the update of the session variable after forward
				session.removeAttribute(SessionVar.MY_AQUARIUM.toString());
			}
		} catch (final ServiceException se) {
			se.logError();
			messages.add(MessageType.ERROR.toString(), se.getError(null));
			saveMessages(req, messages);
			return mapping.findForward(Forward.FAIL);
		}

		saveMessages(req, messages);
		return mapping.findForward(Forward.SUCCESS);
	}

	private SaveAction getSaveAction(final AquariumPoissonVO apVO) {
		// if there's something to commit
		if (apVO.getQuantityNotSaved() != 0) {
			// if the fish doesn't exist in the database
			if (apVO.getQuantitySaved() == 0)
				return SaveAction.ADD;
			else if (apVO.getQuantitySaved() + apVO.getQuantityNotSaved() == 0)
				return SaveAction.REMOVE;
			else
				return SaveAction.UPDATE;
		}
		return SaveAction.NONE;
	}
}
