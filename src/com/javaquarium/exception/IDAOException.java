package com.javaquarium.exception;

/**
 * Interface to describe a classic DAO Exception
 * 
 * @author Valentin
 *
 */
public interface IDAOException {

	/**
	 * Classic getter
	 * 
	 * @return The error code
	 */
	int getCode();

	/**
	 * Classic getter
	 * 
	 * @return The message key
	 */
	String getMessageKey();

	/**
	 * Classic getter
	 * 
	 * @return The cause object of this exception
	 */
	Object getCauseObject();

	/**
	 * Method to log clearly the error for developers
	 */
	void logError();

}
